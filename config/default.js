module.exports = {
  db: {
    url: "mongodb://localhost:27017/donut_dev",
    dbName: "donut_dev"
  },
  dbConfig: {
    user: "postgres",
    password: "postgres",
    database: "donut_dev",
    host: "localhost"
  },
  session: {
    jwtSigner:
      "u6sbQS9/2XZ63APNcv0aLRxUtp3rlm64WoRzbiLfz6GaPckiGXq2BK7QNis4qQfc",
    maxAge: 12 * 3600 * 1000,
    secure: false
  },
  superUser: {
    id: "chihyulin@libgirl.com",
    name: "Chih-Yu Lin"
  }
};
