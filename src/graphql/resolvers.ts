import { IResolverObject } from "graphql-tools";
import { canSeeModel } from "../utils/level";
import { IUserDocument } from "../models/User";
import Model, { IModelDocument } from "../models/Model";

function transformErrors({ errors }: any) {
  return errors.reduce((acc: object, val: any) => {
    const { path, message } = val;
    return {
      ...acc,
      [path]: message
    };
  }, {});
}

const mapLevel = (level: number) => {
  switch (level) {
    case 0:
      return "DEFAULT";
    case 1:
      return "USER";
    case 2:
      return "MAINTAINER";
    case 3:
      return "ADMIN";
    case 4:
      return "SUPER";
    default:
      return null;
  }
};

const resolvers: IResolverObject = {
  User: {
    tmLevel: (parent: IUserDocument) => mapLevel(parent.tmLevel),
    mmLevel: (parent: IUserDocument) => mapLevel(parent.mmLevel),
    dmLevel: (parent: IUserDocument) => mapLevel(parent.dmLevel)
  },
  Model: {
    datasetId: (parent: IModelDocument) => parent?.datasetId?.id
  },
  Query: {
    hello: (_parent, _args) => {
      return "Hello, world!";
    },
    user: (_parent, _args, { user }) => user,
    models: (_parent, _args, { user }) => {
      // if (!canSeeModel(user.mmLevel)) return null;
      return Model.listWithAssocs({});
    }
  }
};

export default resolvers;
