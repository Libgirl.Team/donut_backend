import typeDefs from "./schema";
import resolvers from "./resolvers";
import typeResolvers from "./typeResolvers";
import { ApolloServer } from "apollo-server-express";
import snakeCase from "lodash/snakeCase";

export default new ApolloServer({
  typeDefs,
  // Handle snake_case keys during schema resolution
  fieldResolver: (source, args, contextValue, info) => {
    return source[info.fieldName] || source[snakeCase(info.fieldName)];
  },
  context: async ({ req, res }) => {
    return {
      ip: req.headers["x-forwarded-for"] || req.connection.remoteAddress,
      user: res.locals.user
    };
  },
  resolvers: { ...resolvers, ...typeResolvers },
  playground: true
});
