import { gql } from "apollo-server";

export default gql`
  scalar Date
  scalar DateTime
  scalar JSON

  enum ManagementLevel {
    DEFAULT
    USER
    MAINTAINER
    ADMIN
    SUPER
  }

  type Evaluation {
    id: ID!
    typeId: ID!
    value: Float!
  }

  type Efficiency {
    id: ID!
    typeId: ID!
    value: Float!
  }

  type Model {
    id: ID!
    modelURI: String
    description: String
    ownerId: ID!
    authorId: ID
    previousModelId: ID
    parameters: String
    dataset: String
    evaluations: [Evaluation!]!
    efficiencies: [Efficiency!]!
    modelTypeId: ID!
  }

  type Query {
    hello: String!
    user: User
    models: [Model!]!
  }

  type User {
    id: ID!
    email: String!
    name: String!
    tmLevel: ManagementLevel!
    dmLevel: ManagementLevel!
    mmLevel: ManagementLevel!
  }
`;
