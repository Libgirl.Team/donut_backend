import { Model as MongooseModel, Document, Schema, model } from "mongoose";
import foreignKey from "../utils/foreignKey";
import User from "./User";
import ModelType from "./ModelType";
import Dataset from "./Dataset";

type IEfficiencyDocument = {
  typeId: Schema.Types.ObjectId;
  value: number;
};

type IEvaluationDocument = IEfficiencyDocument;

export interface IModelDocument extends Document {
  modelURI: string;
  description: string;
  ownerId?: Schema.Types.ObjectId;
  authorId?: Schema.Types.ObjectId;

  // These columns belonged to the "ModelSource" subdocument
  previousModelId?: Schema.Types.ObjectId;
  parameters: string;
  datasetId?: Schema.Types.ObjectId;

  evaluations: IEvaluationDocument[];
  efficiencies: IEfficiencyDocument[];

  modelTypeId?: Schema.Types.ObjectId;
}

interface IModelModel extends MongooseModel<IModelDocument> {
  listWithAssocs(conditions: any): Promise<IModelDocument[]>;
}

const requiredString = {
  type: String,
  required: true
};

const evaluationEfficiencySchema = new Schema({
  typeId: { type: Schema.Types.ObjectId, required: true },
  value: { type: Number, required: true }
});

const modelSchema = new Schema(
  {
    modelURI: requiredString,
    description: requiredString,
    parameters: String,
    datasetId: {
      ...foreignKey(Dataset),
      ref: "Dataset"
    },
    modelTypeId: {
      ...foreignKey(ModelType),
      ref: "ModelType",
      required: true
    },
    ownerId: {
      ...foreignKey(User),
      ref: "User",
      required: true
    },
    authorId: {
      ...foreignKey(User),
      ref: "User"
    },
    previousModelId: {
      ...foreignKey("models"),
      ref: "Model"
    },
    evaluations: [evaluationEfficiencySchema],
    efficiencies: [evaluationEfficiencySchema]
  },
  { timestamps: true }
);

modelSchema.statics.listWithAssocs = async (conditions: any) => {
  return Model.find(conditions).populate("ownerId");
};

const Model: IModelModel = model<IModelDocument, IModelModel>(
  "Model",
  modelSchema
);

export default Model;
