import { Model, Document, Schema, model } from "mongoose";
import foreignKey from "../utils/foreignKey";
import User from "./User";

export interface IDatasetDocument extends Document {
  description: string;
  ownerId?: Schema.Types.ObjectId;
  name: string;
  datasetURI: string;
}

interface IDatasetModel extends Model<IDatasetDocument> {}

const requiredString = {
  type: String,
  required: true
};

const datasetSchema = new Schema(
  {
    datasetURI: requiredString,
    description: requiredString,
    ownerId: {
      ...foreignKey(User),
      ref: "User",
      required: true
    }
  },
  { timestamps: true }
);

const Dataset: IDatasetModel = model<IDatasetDocument, IDatasetModel>(
  "Dataset",
  datasetSchema
);

export default Dataset;
