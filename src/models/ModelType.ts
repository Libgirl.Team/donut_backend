import { Model as MongooseModel, Document, Schema, model } from "mongoose";
import foreignKey from "../utils/foreignKey";
import User from "./User";

export interface IModelTypeDocument extends Document {
  name: string;
  description: string;
  ownerId?: Schema.Types.ObjectId;
}

interface IModelTypeModel extends MongooseModel<IModelTypeDocument> {}

const modelTypeSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true
    },
    ownerId: {
      ...foreignKey(User),
      ref: "User",
      required: true
    },
    description: {
      type: String
    }
  },
  { timestamps: true }
);

const ModelType: IModelTypeModel = model<IModelTypeDocument, IModelTypeModel>(
  "ModelType",
  modelTypeSchema
);

export default ModelType;
