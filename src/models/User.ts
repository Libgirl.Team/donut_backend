import { Model, Document, Schema, model } from "mongoose";
import Token from "../utils/token";

enum ManagementLevel {
  Default = 0,
  User,
  Maintainer,
  Admin,
  Super
}

export interface IUserDocument extends Document {
  email: string;
  name: string;
  active: boolean;
  mmLevel: ManagementLevel;
  tmLevel: ManagementLevel;
  dmLevel: ManagementLevel;
}

interface IUserModel extends Model<IUserDocument> {
  findOrCreate(email: string, name: string): Promise<IUserDocument | null>;
  authenticateByJwt(token: string): Promise<IUserDocument | null>;
}

const levelOptions = {
  type: Number,
  default: 0,
  min: [0, "Level must be between 0..4"],
  max: [4, "Level must be between 0..4"]
};

const userSchema = new Schema(
  {
    email: { type: String, unique: true },
    name: { type: String, required: true },
    active: { type: Boolean, required: true, default: true },
    mmLevel: levelOptions,
    tmLevel: levelOptions,
    dmLevel: levelOptions
  },
  { timestamps: true }
);

userSchema.statics.findOrCreate = async (
  email: string,
  name: string
): Promise<IUserDocument | null> => {
  const user = await User.findOne({ email });
  if (user) {
    user.name = name;
    await user.save();
    return user;
  } else {
    return User.create({ email, name });
  }
};

userSchema.statics.authenticateByJwt = async (
  token: string
): Promise<IUserDocument | null> => {
  const payload = await Token.verify(token);
  if (!payload) return null;
  return User.findById(payload.sub);
};

const User: IUserModel = model<IUserDocument, IUserModel>("User", userSchema);

export default User;
