let express = require("express");
let cors = require("cors");

const app = express();

import logger from "morgan";
import helmet from "helmet";
import * as auth from "./utils/auth";
import * as root from "./utils/home";
import errorHandler from "errorhandler";
import gqlServer from "./graphql";
import fetchUser from "./middleware/fetchUser";
import cookieParser from "cookie-parser";
import config from "config";
import GCPClient from "./auth/gcpClient";
import mongoose from "mongoose";

// Network settings
app.use(cors({ credentials: true, origin: "http://localhost:5000" }));
app.use(express.json());
app.use(express.static("public"));
app.use(cookieParser());

if (app.get("env") === "production") {
  app.set("trust proxy", 1); // trust first proxy
  auth.session_setting.cookie.secure = true; // serve secure cookies
}
app.use(logger("dev"));
app.use(auth.session(auth.session_setting));
app.use(helmet());

app.set("port", process.env.PORT || 3000);

app.get("/", root.index);
app.get(auth.redirect_path, GCPClient.callback);

// All the routes afterward requires authorization
app.use(fetchUser);
gqlServer.applyMiddleware({
  app,
  path: "/api",
  cors: {
    origin: true,
    credentials: true
  }
});

// // Account (Team Management)
// app.get("/v1/account/list", account.getAccountList);
// app.put("/v1/account/level", account.putAccountLevel);
// app.get("/v1/account", account.getAccount, auth.selfInfo);
// app.delete("/v1/account", account.deleteAccount);

// // Model (Model Management)
// app.get("/v1/models", model.getModelList);
// app.get("/v1/models/:id", model.getModel);
// app.post("/v1/models", model.registerModel);
// app.put("/v1/models/:id", model.modifyModel);
// app.delete("/v1/models/:id", model.unregisterModel);

// // Dictionary definitions (Model Management)
// app.get("/v1/dict/modeltype/list", dict.getModelTypeList);
// app.get("/v1/dict/modeltype", dict.getModelType);
// app.post("/v1/dict/modeltype", dict.postModelType);
// app.put("/v1/dict/modeltype", dict.putModelType);
// app.delete("/v1/dict/modeltype", dict.deleteModelType);

// app.get("/v1/dict/algorithm/list", dict.getAlgorithmList);
// app.get("/v1/dict/algorithm", dict.getAlgorithm);
// app.post("/v1/dict/algorithm", dict.postAlgorithm);
// app.put("/v1/dict/algorithm", dict.putAlgorithm);
// app.delete("/v1/dict/algorithm", dict.deleteAlgorithm);

// app.get("/v1/dict/usage/list", dict.getUsageList);
// app.get("/v1/dict/usage", dict.getUsage);
// app.post("/v1/dict/usage", dict.postUsage);
// app.put("/v1/dict/usage", dict.putUsage);
// app.delete("/v1/dict/usage", dict.deleteUsage);

// app.get("/v1/dict/datatype/list", dict.getDataTypeList);
// app.get("/v1/dict/datatype", dict.getDataType);
// app.post("/v1/dict/datatype", dict.postDataType);
// app.put("/v1/dict/datatype", dict.putDataType);
// app.delete("/v1/dict/datatype", dict.deleteDataType);

// app.get("/v1/dict/evaluation/list", dict.getEvaluationList);
// app.get("/v1/dict/evaluation", dict.getEvaluation);
// app.post("/v1/dict/evaluation", dict.postEvaluation);
// app.put("/v1/dict/evaluation", dict.putEvaluation);
// app.delete("/v1/dict/evaluation", dict.deleteEvaluation);

// app.get("/v1/dict/efficiency/list", dict.getEfficiencyList);
// app.get("/v1/dict/efficiency", dict.getEfficiency);
// app.post("/v1/dict/efficiency", dict.postEfficiency);
// app.put("/v1/dict/efficiency", dict.putEfficiency);
// app.delete("/v1/dict/efficiency", dict.deleteEfficiency);

// app.get("/v1/dict/dataset/list", dict.getDatasetList);
// app.get("/v1/dict/dataset", dict.getDataset);
// app.post("/v1/dict/dataset", dict.postDataset);
// app.put("/v1/dict/dataset", dict.putDataset);
// app.delete("/v1/dict/dataset", dict.deleteDataset);

// OAuth 2.0
app.post("/v1/oauth/logout", GCPClient.logout);

app.use(errorHandler());

mongoose
  .connect(config.get("db.url"), {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  })
  .then(() => {
    mongoose.set("debug", process.env.NODE_ENV === "development");
    app.listen(app.get("port"), () => {
      const port = app.get("port");
      const env = app.get("env");
      console.log(
        `  App is running at http://localhost:${port} in ${env} mode\n` +
          `  Press CTRL-C to stop\n`
      );
    });
  });
