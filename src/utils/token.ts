import config from "config";
import jwt from "jsonwebtoken";
import User, { IUserDocument } from "../models/User";

export interface ITokenPayload {
  sub: string | number;
}

export default class Token {
  static issue(user: IUserDocument) {
    return jwt.sign({ sub: user._id }, config.get("session.jwtSigner"), {
      expiresIn: Number(config.get("session.maxAge")) / 1000
    });
  }

  static verify(token: string): Promise<null | ITokenPayload> {
    return new Promise(resolve => {
      jwt.verify(token, config.get("session.jwtSigner"), {}, (err, payload) => {
        if (err) resolve(null);
        resolve(payload as ITokenPayload);
      });
    });
  }
}
