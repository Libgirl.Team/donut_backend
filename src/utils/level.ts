/*
  Level number to Level Label:
  0 = 'Default'
  1 = 'User'
  2 = 'Maintainer'
  3 = 'Admin'
  4 = 'Super'
  */
export type TMLv = 0 | 1 | 2 | 3 | 4;
export type MMLv = 0 | 1 | 2 | 3;
export type DMLv = 0 | 1 | 2 | 3;
type OtherLv = 0 | 1 | 2 | 3; // for generic function on both MMLv & DMLv

export type Levels = {
    tm: TMLv,
    mm: MMLv,
    dm: DMLv,
}

export function areValidLevels(lvs: Levels): boolean {
    if (lvs.tm == 0 && (lvs.mm !== 0 || lvs.dm !== 0)) {
        return false;
    } else if (lvs.tm >= 2 && (lvs.mm < lvs.tm || lvs.dm < lvs.tm)) {
        return false;
    } else {
        return true;
    }
}

function areEqLevels(x: Levels, y: Levels) {
    if (x.tm == y.tm && x.mm == y.mm && x.dm == y.dm) {
        return true;
    } else {
        return false;
    }
}

function canSetTM(user: TMLv, original: TMLv, final: TMLv): boolean {
    if (original == final) {
        return true;
    } else {
        switch (user) {
            case 4:
            case 3:
                if (original <= 3 && final <= 3) {
                    return true;
                } else {
                    return false;
                }
            case 2:
                if (original <= 1) {
                    if (final <= 2) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            default: return false;
        }
    }
}

function updateTM(original: Levels, finalTM: TMLv): Levels {
    switch (finalTM) {
        case 4:
            throw new Error('set newTM to 4.')
        case 3:
            return { tm: 3, mm: 3, dm: 3 };
        case 2:
            return {
                tm: 2,
                mm: (finalTM <= original.mm) ? original.mm : 2,
                dm: (finalTM <= original.mm) ? original.dm : 2,
            };
        case 1:
            return { tm: finalTM, mm: original.mm, dm: original.dm };
        case 0: {
            return { tm: 0, mm: 0, dm: 0 };
        }
    }
}

function canSetOtherLv(user: OtherLv, original: OtherLv, final: OtherLv): boolean {
    if (original == final) {
        return true;
    } else {
        switch (user) {
            case 3: return true;
            case 2:
                if (original <= 1) {
                    if (final <= 2) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            default: return false;
        }
    }
}

function mmToOther(mm: MMLv): OtherLv {
    switch (mm) {
        case 0: return 0;
        case 1: return 1;
        case 2: return 2;
        case 3: return 3;
    }
}

function dmToOther(dm: DMLv): OtherLv {
    switch (dm) {
        case 0: return 0;
        case 1: return 1;
        case 2: return 2;
        case 3: return 3;
    }
}

function canSetMM(user: MMLv, original: MMLv, final: MMLv): boolean {
    return canSetOtherLv(mmToOther(user), mmToOther(original), mmToOther(final));
}

function canSetDM(user: DMLv, original: DMLv, final: DMLv): boolean {
    return canSetOtherLv(dmToOther(user), dmToOther(original), dmToOther(final));
}

// Checking functions
export function canSetLevels(user: Levels, original: Levels, final: Levels): boolean {
    if (areValidLevels(user) && areValidLevels(original) && areValidLevels(final) && !areEqLevels(original, final)) {
        if (canSetTM(user.tm, original.tm, final.tm)) {
            let tmpLvs = updateTM(original, final.tm);
            if (canSetMM(user.mm, tmpLvs.mm, final.mm)) {
                // tmpLvs = updateMM(tmpLvs, final.mm);
                if (canSetDM(user.dm, tmpLvs.dm, final.dm)) {
                    // tmpLvs = updateDM(tmpLvs, final.dm);
                    return true;
                }
            }
        }
    }
    return false;
}

export function canSeeAccount(user: TMLv): boolean {
    if (user >= 1) {
        return true;
    } else {
        return false;
    }
}

export function canDeleteAccount(user: TMLv, target: TMLv): boolean {
    if (target == 4) {
        return false;
    } if (user >= 3) {
        return true;
    } else if (user == 2 && target <= 1) {
        return true;
    } else {
        return false;
    }
}

export function canRegisterModel(user: MMLv): boolean {
    if (user >= 1) {
        return true;
    } else {
        return false;
    }
}

export function canDeleteEndpoint(user: DMLv, is_owner: boolean): boolean {
    if (user >= 2) {
        return true;
    } else if (user == 1 && is_owner) {
        return true;
    } else {
        return false;
    }
}

export function canDeleteModel(user: MMLv, is_owner: boolean): boolean {
    if (user >= 2) {
        return true;
    } else if (user == 1 && is_owner) {
        return true;
    } else {
        return false;
    }
}

export function canSeeModel(user: MMLv): boolean {
    if (user >= 1) {
        return true;
    } else {
        return false;
    }
}

export function canModifyModel(user: MMLv): boolean {
    return canSeeModel(user);
}