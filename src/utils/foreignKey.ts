import mongoose, { Model, Schema } from "mongoose";

// What am I doing with my life????
// https://stackoverflow.com/questions/56402942/there-is-a-way-to-enforce-referential-integrity-in-mongodb

export default (modelOrCollectionName: Model<any, any> | string) => ({
  type: Schema.Types.ObjectId,
  validate: generateValidator(modelOrCollectionName)
});

function generateValidator(modelOrCollectionName: Model<any, any> | string) {
  if (modelOrCollectionName instanceof String)
    return validateReferenceLowLevel(modelOrCollectionName as string);
  return validateReferenceByModel(modelOrCollectionName as Model<any, any>);
}

export const validateReferenceByModel = (referencedModel: Model<any, any>) => ({
  validator: async (id: Schema.Types.ObjectId | null) => {
    return (
      !id || (await referencedModel.findById(id, (err, rec) => rec !== null))
    );
  },
  message: "The referenced document does not exist"
});

export const validateReferenceLowLevel = (
  referencedCollectionName: string
) => ({
  validator: async (_id: Schema.Types.ObjectId | null) => {
    return (
      !_id ||
      (await mongoose.connection.db
        .collection(referencedCollectionName)
        .findOne({ _id })) !== null
    );
  }
});
