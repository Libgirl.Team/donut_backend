import mongodb from "mongodb";
import config from "config";
import { Account } from "./auth";

export enum Collection {
  Accounts = "Account_Repo",
  Models = "Model_Repo",
  ModelTypes = "ModelType_Dict",
  Algorithms = "Algorithm_Dict",
  Usage = "Usage_Dict",
  DataTypes = "DataType_Dict",
  Evaluation = "Evaluation_Dict",
  Eficciency = "Efficiency_Dict",
  Datasets = "Dataset_Dict"
}

export const CollectionNames = Object.values(Collection);

class DBConnection {
  public connection?: mongodb.MongoClient;
  public db?: mongodb.Db;

  async connect() {
    this.connection = await mongodb.connect(config.get("db.url"));
    this.db = this.connection.db(config.get("db.dbName"));
  }

  collection(name: Collection): mongodb.Collection {
    return this.db?.collection(name) as mongodb.Collection;
  }

  defaultSuperUser() {
    return new Account(
      config.get("superUser.id"),
      config.get("superUser.name"),
      true,
      4,
      3,
      3
    );
  }

  async createSuperAccount() {
    const account_info = await this.db
      ?.collection(Collection.Accounts)
      .findOne({ ID: config.get("superUser.id") }, { projection: { _id: 0 } });
    if (!account_info) {
      await this.db
        ?.collection(Collection.Accounts)
        .insertOne(this.defaultSuperUser);
    }
  }

  async createCollection(name: string) {
    return this.db?.createCollection(name);
  }

  getCollection<T>(
    collection: Collection,
    query: mongodb.FilterQuery<any> = {},
    options: mongodb.FindOneOptions = {}
  ): Promise<T[] | any> {
    return new Promise((resolve, reject) => {
      this.collection(collection)
        .find(query, options)
        .toArray((err: any, results: T[]) => {
          if (err) reject(err);
          else resolve(results);
        });
    });
  }

  async synchronizeSchema() {
    CollectionNames.forEach(async name => await this.createCollection(name));
    this.createSuperAccount();
  }
}

export default new DBConnection();
