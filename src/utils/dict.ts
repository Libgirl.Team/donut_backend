import * as db from "./db";
import { Request, Response } from "express";

// Model Type definition
export const getModelTypeList = function (req: Request, res: Response) {
    db.dbGetDefinitionList(req, res, db.model_type_dict);
}

export const getModelType = function (req: Request, res: Response) {
    if (req.query._id) {
        db.dbGetDefinitionByID(req, res, db.model_type_dict);
    } else if (req.query.name) {        
        db.dbGetDefinitionByName(req, res, db.model_type_dict);
    } else {
        res.status(405).send("Method Not Allowed: You should query by _id or name!");
    }
}

export const postModelType = function (req: Request, res: Response) {
    db.dbPostDefinition(req, res, db.model_type_dict);
}

export const putModelType = function (req: Request, res: Response) {
    db.dbPutDefinitionByID(req, res, db.model_type_dict);
}

export const deleteModelType = function (req: Request, res: Response) {
    db.dbDeleteDefinitionByID(req, res, db.model_type_dict);
}

// Algorithm definition
export const getAlgorithmList = function (req: Request, res: Response) {
    db.dbGetDefinitionList(req, res, db.algorithm_dict);
}

export const getAlgorithm = function (req: Request, res: Response) {
    if (req.query._id) {
        db.dbGetDefinitionByID(req, res, db.algorithm_dict);
    } else if (req.query.name) {        
        db.dbGetDefinitionByName(req, res, db.algorithm_dict);
    } else {
        res.status(405).send("Method Not Allowed: You should query by _id or name!");
    }
}

export const postAlgorithm = function (req: Request, res: Response) {
    db.dbPostDefinition(req, res, db.algorithm_dict);
}

export const putAlgorithm = function (req: Request, res: Response) {
    db.dbPutDefinitionByID(req, res, db.algorithm_dict);
}

export const deleteAlgorithm = function (req: Request, res: Response) {
    db.dbDeleteDefinitionByID(req, res, db.algorithm_dict);
}

// Usage definition
export const getUsageList = function (req: Request, res: Response) {
    db.dbGetDefinitionList(req, res, db.usage_dict);
}

export const getUsage = function (req: Request, res: Response) {
    if (req.query._id) {
        db.dbGetDefinitionByID(req, res, db.usage_dict);
    } else if (req.query.name) {        
        db.dbGetDefinitionByName(req, res, db.usage_dict);
    } else {
        res.status(405).send("Method Not Allowed: You should query by _id or name!");
    }
}

export const postUsage = function (req: Request, res: Response) {
    db.dbPostDefinition(req, res, db.usage_dict);
}

export const putUsage = function (req: Request, res: Response) {
    db.dbPutDefinitionByID(req, res, db.usage_dict);
}

export const deleteUsage = function (req: Request, res: Response) {
    db.dbDeleteDefinitionByID(req, res, db.usage_dict);
}

// DataType Definition
export const getDataTypeList = function (req: Request, res: Response) {
    db.dbGetDefinitionList(req, res, db.data_type_dict);
}

export const getDataType = function (req: Request, res: Response) {
    if (req.query._id) {
        db.dbGetDefinitionByID(req, res, db.data_type_dict);
    } else if (req.query.name) {        
        db.dbGetDefinitionByName(req, res, db.data_type_dict);
    } else {
        res.status(405).send("Method Not Allowed: You should query by _id or name!");
    }
}

export const postDataType = function (req: Request, res: Response) {
    db.dbPostDefinition(req, res, db.data_type_dict);
}

export const putDataType = function (req: Request, res: Response) {
    db.dbPutDefinitionByID(req, res, db.data_type_dict);
}

export const deleteDataType = function (req: Request, res: Response) {
    db.dbDeleteDefinitionByID(req, res, db.data_type_dict);
}

// Evaluation definition
export const getEvaluationList = function (req: Request, res: Response) {
    db.dbGetDefinitionList(req, res, db.evaluation_dict);
}

export const getEvaluation = function (req: Request, res: Response) {
    if (req.query._id) {
        db.dbGetDefinitionByID(req, res, db.evaluation_dict);
    } else if (req.query.name) {        
        db.dbGetDefinitionByName(req, res, db.evaluation_dict);
    } else {
        res.status(405).send("Method Not Allowed: You should query by _id or name!");
    }
}

export const postEvaluation = function (req: Request, res: Response) {
    db.dbPostDefinition(req, res, db.evaluation_dict);
}

export const putEvaluation = function (req: Request, res: Response) {
    db.dbPutDefinitionByID(req, res, db.evaluation_dict);
}

export const deleteEvaluation = function (req: Request, res: Response) {
    db.dbDeleteDefinitionByID(req, res, db.evaluation_dict);
}

// Efficiency definition
export const getEfficiencyList = function (req: Request, res: Response) {
    db.dbGetDefinitionList(req, res, db.efficiency_dict);
}

export const getEfficiency = function (req: Request, res: Response) {
    if (req.query._id) {
        db.dbGetDefinitionByID(req, res, db.efficiency_dict);
    } else if (req.query.name) {        
        db.dbGetDefinitionByName(req, res, db.efficiency_dict);
    } else {
        res.status(405).send("Method Not Allowed: You should query by _id or name!");
    }
}

export const postEfficiency = function (req: Request, res: Response) {
    db.dbPostDefinition(req, res, db.efficiency_dict);
}

export const putEfficiency = function (req: Request, res: Response) {
    db.dbPutDefinitionByID(req, res, db.efficiency_dict);
}

export const deleteEfficiency = function (req: Request, res: Response) {
    db.dbDeleteDefinitionByID(req, res, db.efficiency_dict);
}

// Dataset definition
export const getDatasetList = function (req: Request, res: Response) {
    db.dbGetDefinitionList(req, res, db.dataset_dict);
}

export const getDataset = function (req: Request, res: Response) {
    if (req.query._id) {
        db.dbGetDefinitionByID(req, res, db.dataset_dict);
    } else if (req.query.name) {        
        db.dbGetDefinitionByName(req, res, db.dataset_dict);
    } else {
        res.status(405).send("Method Not Allowed: You should query by _id or name!");
    }
}

export const postDataset = function (req: Request, res: Response) {
    db.dbPostDefinition(req, res, db.dataset_dict);
}

export const putDataset = function (req: Request, res: Response) {
    db.dbPutDefinitionByID(req, res, db.dataset_dict);
}

export const deleteDataset = function (req: Request, res: Response) {
    db.dbDeleteDefinitionByID(req, res, db.dataset_dict);
}
