import { Request, Response } from "express";

/**
 * GET /
 * Root of donut backend server.
 */

export const index = (req: Request, res: Response) => {
    res.send('Root of donut backend server......\n');
};
