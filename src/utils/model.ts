import * as db from "./db";
import { getSelfID } from "./auth";
import { Request, Response } from "express";

export const getModelList = function(req: Request, res: Response) {
  const selfID = getSelfID(res);
  if (Object.keys(req.query).length !== 0) {
    db.dbGetFilteredModel(selfID, req, res);
  } else {
    db.dbGetModelList(selfID, res);
  }
};

export const getModel = function(req: Request, res: Response) {
  console.log(req.query);
  if (!req.query._id) {
    console.log("No _id found!");
    res.status(200).send("");
  } else {
    const selfID = getSelfID(res);
    db.dbGetModel(selfID, req.query._id, res);
  }
};

export const registerModel = function(req: Request, res: Response) {
  console.log("\nPOST /v1/model");
  // console.log(req.body);
  const selfID = getSelfID(res);
  db.dbRegisterModel(selfID, req, res);
};

export const modifyModel = function(req: Request, res: Response) {
  console.log("\nPUT /v1/model,  req.body:");
  console.log(req.body);
  const selfID = getSelfID(res);
  db.dbModifyModelByID(selfID, req, res);
};

export const unregisterModel = function(req: Request, res: Response) {
  console.log("\nDELETE /v1/model, req.body:");
  console.log(req.body);
  const selfID = getSelfID(res);
  db.dbUnRegisterModel(selfID, req, res);
};
