import mongodb from "mongodb";
import { getSelfID } from "./auth";
import dbConnection, { Collection } from "./dbConnection";
import { Request, Response } from "express";
import {
  Levels,
  canSetLevels,
  canDeleteAccount,
  canSeeModel,
  canRegisterModel,
  canDeleteModel,
  canModifyModel,
  canSeeAccount
} from "./level";
import config from "config";

// Database Parameter Settings
const mongo = mongodb.MongoClient;
const ObjectId = mongodb.ObjectId;
const url = config.get("db.url") as string;
const db_name = config.get("db.dbName") as string;
const superID = config.get("superUser.id") as string;

// Definition of collection_set
export const model_type_dict = "ModelType_Dict";
export const algorithm_dict = "Algorithm_Dict";
export const usage_dict = "Usage_Dict";
export const data_type_dict = "DataType_Dict";
export const evaluation_dict = "Evaluation_Dict";
export const efficiency_dict = "Efficiency_Dict";
export const dataset_dict = "Dataset_Dict";

// Account Functions
export const dbGetAccount = async function(
  selfID: string,
  queryID: string,
  res: Response
) {
  // TODO: Determine who can see the account when is_alive is false
  const account_info = await getAccountInfo(selfID);
  if (!canSeeAccount(account_info.TM_Level)) {
    res
      .status(405)
      .send("Your Team Level is NOT enough to see accounts information!");
  } else {
    dbConnection
      .collection(Collection.Accounts)
      .findOne(
        { ID: queryID },
        { projection: { _id: 0 } },
        (err: any, account_info: any) => {
          if (err) throw err;
          console.log(account_info);
          res.status(200).send(account_info);
        }
      );
  }
};

export const dbDeleteAccount = async function(
  selfID: string,
  targetID: string,
  res: Response
) {
  if (targetID == superID) {
    res.status(405).send("Super account should not be changed!");
    return;
  }

  const account_collection = dbConnection.collection(Collection.Accounts);
  const self_res = await account_collection.findOne(
    { ID: selfID },
    { projection: { _id: 0 } }
  );
  const target_res = await account_collection.findOne(
    { ID: targetID },
    { projection: { _id: 0 } }
  );

  if (target_res == null || target_res.is_alive == false) {
    res.status(404).send("Account not exists!");
  } else if (canDeleteAccount(self_res.TM_Level, target_res.TM_Level)) {
    await account_collection.updateOne(
      { ID: targetID },
      { $set: { is_alive: false } }
    );
    res.status(200).send("Successfully delete account!");
  } else {
    res
      .status(405)
      .send("Your Team Level is NOT enough to delete this account!");
  }
};

export const dbGetAccountList = async function(selfID: string, res: Response) {
  const account_info = await getAccountInfo(selfID);

  if (!canSeeAccount(account_info.TM_Level)) {
    res
      .status(405)
      .send("Your Team Level is NOT enough to see account information!");
  } else {
    // TODO: Determine who can see the accounts when the is_alive is false
    dbConnection
      .collection(Collection.Accounts)
      .find({}, { projection: { _id: 0 } })
      .toArray((dberr: any, account_info: any) => {
        if (dberr) throw dberr;
        if (account_info) {
          res.status(200).send(account_info);
        }
      });
  }
};

export const dbPutLevel = async function(
  targetID: string,
  req: Request,
  res: Response
) {
  if (targetID == superID) {
    res.status(405).send("Super account should not be changed!");
    return;
  }
  const selfID = getSelfID(res);
  const account_collection = dbConnection.collection(Collection.Accounts);
  const self_res = await account_collection.findOne(
    { ID: selfID },
    { projection: { _id: 0 } }
  );
  const origin_res = await account_collection.findOne(
    { ID: targetID },
    { projection: { _id: 0 } }
  );

  let self: Levels = {
    tm: self_res.TM_Level,
    mm: self_res.MM_Level,
    dm: self_res.DM_Level
  };
  let origin: Levels = {
    tm: origin_res.TM_Level,
    mm: origin_res.MM_Level,
    dm: origin_res.DM_Level
  };
  let target: Levels = {
    tm: req.body.TM_Level,
    mm: req.body.MM_Level,
    dm: req.body.DM_Level
  };

  console.log(self, origin, target);
  if (canSetLevels(self, origin, target)) {
    await account_collection.updateOne(
      { ID: targetID },
      {
        $set: { TM_Level: target.tm, MM_Level: target.mm, DM_Level: target.dm }
      }
    );
    res.status(200).send("Successfully update account levels");
  } else {
    console.log("Not valid level setting");
    res.status(400).send("Invalid info value");
  }
};

// Model Functions
export const dbGetModel = async function(
  selfID: string,
  queryID: string,
  res: Response
) {
  const account_info = await getAccountInfo(selfID);
  if (!canSeeModel(account_info.MM_Level)) {
    res
      .status(405)
      .send("Your Model Level is NOT enough to see model information!");
  } else {
    dbConnection
      .collection(Collection.Models)
      .findOne(
        { _id: new ObjectId(queryID) },
        {},
        (err: any, model_info: any) => {
          if (err) throw err;
          console.log(model_info);
          res.status(200).send(model_info);
        }
      );
  }
};

export const dbGetFilteredModel = async function(
  selfID: string,
  req: Request,
  res: Response
) {
  const account_info = await getAccountInfo(selfID);
  if (!canSeeModel(account_info.MM_Level)) {
    res
      .status(405)
      .send("Your Model Level is NOT enough to see model information!");
  } else {
    let q = req.query;
    dbConnection
      .collection(Collection.Models)
      .find(q, {})
      .toArray((err: any, model_info: any) => {
        if (err) throw err;
        console.log(model_info);
        res.status(200).send(model_info);
      });
  }
};

export const dbRegisterModel = async function(
  selfID: string,
  req: Request,
  res: Response
) {
  if (!req.body.model_knowledge || !req.body.model_knowledge.intrinsics) {
    res
      .status(400)
      .send("The column of model_knowledge/intrinsics must be specified!");
    return;
  }
  const db = await mongo.connect(url);
  const dbase = db.db(db_name);
  const account_info = await getAccountInfo(selfID);
  if (!canRegisterModel(account_info.MM_Level)) {
    db.close();
    res.status(405).send("Method Not Allowed!");
  } else {
    const modeltype_collection = dbase.collection(model_type_dict);
    const evaluation_collection = dbase.collection(evaluation_dict);
    const efficiency_collection = dbase.collection(efficiency_dict);
    const model_collection = dbase.collection(Collection.Models);
    const dataset_collection = dbase.collection(dataset_dict);
    let major_num = req.body.major_number;
    let model_type = req.body.model_knowledge.intrinsics.model_type;
    let evaluations = req.body.model_knowledge.summary.evaluations;
    let efficiencies = req.body.model_knowledge.summary.efficiencies;
    let dataset = req.body.model_knowledge.source.dataset;
    let prev_model = req.body.model_knowledge.source.prev_model;
    if (!model_type) {
      res.status(400).send("model_type is not specified!");
    } else if (!mongodb.ObjectID.isValid(model_type)) {
      res.status(400).send("model_type hash is not valid!");
    } else {
      let modeltype_info = await modeltype_collection.findOne({
        _id: new ObjectId(model_type)
      });
      if (!modeltype_info) {
        res.status(400).send("model_type hash is not found in the dictionary!");
        return;
      }
      if (!major_num && major_num != 0) {
        res.status(400).send("major_num is not defined in the request!");
        return;
      }
      console.log("new_major = " + modeltype_info.versions.new_major);
      console.log("major_num = " + major_num);
      if (major_num > modeltype_info.versions.new_major) {
        res.status(400).send("The major version is NOT valid!");
        return;
      }
      if (prev_model) {
        if (!mongodb.ObjectID.isValid(prev_model)) {
          res.status(400).send("prev_model hash is not valid!");
          return;
        } else {
          let prev_model_info = await model_collection.findOne({
            _id: new ObjectId(prev_model)
          });
          if (!prev_model_info) {
            res
              .status(400)
              .send("prev_model hash is not found in the Collection.Models!");
            return;
          }
        }
      }
      if (dataset) {
        if (!mongodb.ObjectID.isValid(dataset)) {
          res.status(400).send("dataset hash is not valid!");
          return;
        }
        let dataset_info = await dataset_collection.findOne({
          _id: new ObjectId(dataset)
        });
        if (!dataset_info) {
          res.status(400).send("dataset hash is not found in the dictionary!");
          return;
        }
      }

      for (const index in evaluations) {
        if (!mongodb.ObjectID.isValid(index)) {
          res.status(400).send("Evaluation hash " + index + " was not valid!");
          return;
        }
        let evaluation_info = await evaluation_collection.findOne({
          _id: new ObjectId(index)
        });
        if (!evaluation_info) {
          res
            .status(400)
            .send("Evaluation hash is not found in the dictionary!");
          return;
        }
      }
      for (const index in efficiencies) {
        if (!mongodb.ObjectID.isValid(index)) {
          res.status(400).send("Efficiency hash " + index + " was not valid!");
          return;
        }
        let efficiency_info = await efficiency_collection.findOne({
          _id: new ObjectId(index)
        });
        if (!efficiency_info) {
          res
            .status(400)
            .send("Efficiency hash is not found in the dictionary!");
          return;
        }
      }
      // Insert model and update versions
      model_collection.insertOne(
        { model_knowledge: req.body.model_knowledge },
        async (dberr: any, dbres: any) => {
          if (dberr) throw dberr;
          console.log("Insert succeeded!");
          const result_id = dbres.ops[0]._id;
          //console.log(modeltype_info.versions);
          if (major_num == modeltype_info.versions.new_major) {
            ++modeltype_info.versions.new_major;
            let major_instance = {
              new_minor: 1,
              minor_versions: {
                "0": result_id.toString()
              }
            };
            modeltype_info.versions.major_versions[
              major_num.toString()
            ] = major_instance;
          } else {
            let major_instance =
              modeltype_info.versions.major_versions[major_num.toString()];
            const new_minor = major_instance.new_minor;
            major_instance.minor_versions[
              new_minor.toString()
            ] = result_id.toString();
            ++major_instance.new_minor;
            console.log(modeltype_info.versions.major_versions);
          }

          await modeltype_collection.updateOne(
            { _id: new ObjectId(model_type) },
            { $set: { versions: modeltype_info.versions } }
          );
          db.close();
          res.status(200).send(dbres.ops[0]);
        }
      );
    }
  }
};

export const dbModifyModelByID = async function(
  selfID: string,
  req: Request,
  res: Response
) {
  const _id = req.body._id;
  if (!mongodb.ObjectID.isValid(_id)) {
    res.status(400).send("Not valid _id!");
  } else {
    const db = await mongo.connect(url);
    const dbase = await db.db(db_name);
    const account_info = await getAccountInfo(selfID);
    const target_collection = dbase.collection(Collection.Models);
    const model_info = await target_collection.findOne(
      { _id: new ObjectId(_id) },
      { projection: { _id: 0 } }
    );
    if (!model_info) {
      db.close();
      res.status(404).send("model not found!");
      return;
    }
    console.log("account_info.ID = " + account_info.ID);
    console.log("model_info.owner_id = " + model_info.owner_id + "\n");

    if (canModifyModel(account_info.MM_Level)) {
      let target_model = req.body;
      delete target_model["_id"];

      // If name exists, check duplication
      if (!target_model.name) {
        // Check validation of hash if exists
        if (await checkValidationHash(dbase, target_model, Collection.Models)) {
          await target_collection.updateOne(
            { _id: new ObjectId(_id) },
            { $set: target_model },
            (dberr: any, dbres: any) => {
              db.close();
              console.log("Update Succeeded!");
              res.status(200).send("Update Succeeded!");
            }
          );
        } else {
          db.close();
          res.status(405).send("Target model hash is not valid!");
        }
      } else {
        db.close();
        res.status(405).send("Duplication name already exists!");
      }
    } else {
      db.close();
      res.status(405).send("Method not allowed!");
    }
  }
};

export const dbUnRegisterModel = async function(
  selfID: string,
  req: Request,
  res: Response
) {
  const model_ID = req.query._id;
  const db = await mongo.connect(url);
  const dbase = db.db(db_name);
  const model_repo_collection = dbase.collection(Collection.Models);
  const model_type_collection = dbase.collection(model_type_dict);
  const account_info = await getAccountInfo(selfID);
  const model_info = await model_repo_collection.findOne({
    _id: new ObjectId(model_ID)
  });

  if (!model_info) {
    db.close();
    res.status(404).send("Definition not found!");
    return;
  }
  const model_type_id = model_info.model_knowledge.intrinsics.model_type;
  const model_type_document = await model_type_collection.findOne({
    _id: new ObjectId(model_type_id)
  });

  if (
    canDeleteModel(
      account_info.MM_Level,
      account_info.ID == model_info.model_knowledge.owner_id
    )
  ) {
    const ver: string = getVersion(model_type_document, model_ID);
    console.log("Deleting model type link hash: " + ver + " ......");

    await model_type_collection.updateOne(
      { _id: new ObjectId(model_type_id) },
      { $unset: { [ver]: 1 } }
    );
    await model_repo_collection.deleteOne(
      { _id: new ObjectId(model_ID) },
      (dberr: any, db_res: any) => {
        if (dberr) throw dberr;
        db.close();
        res.status(200).send("Delete Succeeded!");
      }
    );
  } else {
    db.close();
    res.status(405).send("Method not allowed!");
  }
};

export const dbGetModelList = async function(selfID: string, res: Response) {
  const account_info = await getAccountInfo(selfID);
  if (!canSeeModel(account_info.MM_Level)) {
    res
      .status(405)
      .send("Your Model Level is NOT enough to see model information!");
  } else {
    dbConnection
      .collection(Collection.Models)
      .find({}, {})
      .toArray((dberr: any, model_info: any) => {
        if (dberr) throw dberr;
        if (model_info) {
          res.status(200).send(model_info);
        }
      });
  }
};

// Definition Functions
export const dbGetDefinitionList = async function(
  req: Request,
  res: Response,
  coll: string
) {
  const selfID = getSelfID(res);
  const account_info = await getAccountInfo(selfID);
  if (canSeeModel(account_info.MM_Level)) {
    dbConnection
      .collection(coll as Collection)
      .find({}, {})
      .toArray((dberr: any, info: any) => {
        if (dberr) throw dberr;
        if (info) {
          res.status(200).send(info);
        }
      });
  } else {
    res
      .status(405)
      .send("Your Model Level is NOT enough to see model information!");
  }
};

export const dbGetDefinitionByID = async function(
  req: Request,
  res: Response,
  coll: string
) {
  const selfID = getSelfID(res);
  const _id = req.query._id;
  if (!mongodb.ObjectID.isValid(_id)) {
    res.status(400).send("Not valid _id!");
  } else {
    const account_info = await getAccountInfo(selfID);
    if (canSeeModel(account_info.MM_Level)) {
      mongo.connect(url, function(err: any, db: any) {
        if (err) throw err;
        dbConnection
          .collection(coll as Collection)
          .findOne({ _id: new ObjectId(_id) }, {}, (dberr: any, info: any) => {
            if (dberr) {
              console.log(err);
              res.status(400).send(info);
            }
            db.close();
            if (info) {
              res.status(200).send(info);
            }
          });
      });
    } else {
      res
        .status(405)
        .send("Your Model Level is NOT enough to see model information!");
    }
  }
};

export const dbGetDefinitionByName = async function(
  req: Request,
  res: Response,
  coll: string
) {
  console.log("getDefinitionByName");
  console.log(req.query.name);
  const selfID = getSelfID(res);
  const db = await mongo.connect(url);
  const dbase = db.db(db_name);
  const account_info = await getAccountInfo(selfID);
  if (canSeeModel(account_info.MM_Level)) {
    mongo.connect(url, function(err: any, db: any) {
      if (err) throw err;
      dbase
        .collection(coll)
        .findOne({ name: req.query.name }, {}, (dberr: any, info: any) => {
          if (dberr) {
            res.status(400).send(info);
          }
          if (info) {
            res.status(200).send(info);
          } else {
            res.status(200).send("");
          }
          db.close();
        });
    });
  } else {
    db.close();
    res
      .status(405)
      .send("Your Model Level is NOT enough to see model information!");
  }
};

export const dbPostDefinition = async function(
  req: Request,
  res: Response,
  coll: string
) {
  const selfID = getSelfID(res);
  if (!req.body.name) {
    res.status(400).send("The column of name must be specified!");
    return;
  }
  const db = await mongo.connect(url);
  const dbase = db.db(db_name);
  const account_info = await getAccountInfo(selfID);

  if (!canRegisterModel(account_info.MM_Level)) {
    db.close();
    res.status(405).send("Method Not Allowed!");
  } else {
    const target_collection = dbase.collection(coll);
    const coll_info = await target_collection.findOne({ name: req.body.name });
    if (coll_info) {
      db.close();
      res.status(409).send(req.body.name + " already exists!");
      return;
    } else if (coll == usage_dict) {
      let in_type = req.body.input_type;
      let out_type = req.body.output_type;
      if (!in_type || !out_type) {
        res
          .status(400)
          .send("Input_type/Output_type is not found in the request body!");
        return;
      } else if (
        !mongodb.ObjectID.isValid(in_type) ||
        !mongodb.ObjectID.isValid(out_type)
      ) {
        res.status(400).send("Input_type/Output_type hash is not valid!");
        return;
      } else {
        const datatype_collection = dbase.collection(data_type_dict);
        const in_type_info = await datatype_collection.findOne({
          _id: new ObjectId(in_type)
        });
        const out_type_info = await datatype_collection.findOne({
          _id: new ObjectId(out_type)
        });
        if (!in_type_info || !out_type_info) {
          res
            .status(400)
            .send("Input_type/Output_type is not found in the DataType_Dict!");
          return;
        }
      }
    } else if (coll == model_type_dict) {
      let algorithm_hash = req.body.algorithm;
      let usage_hash = req.body.usage;
      if (!algorithm_hash || !usage_hash) {
        res
          .status(400)
          .send("Algorithm/Usage hash is not found in the request body!");
        return;
      } else if (
        !mongodb.ObjectID.isValid(algorithm_hash) ||
        !mongodb.ObjectID.isValid(usage_hash)
      ) {
        res.status(400).send("Algorithm/Usage hash is not valid!");
        return;
      } else {
        const algorithm_collection = dbase.collection(algorithm_dict);
        const usage_collection = dbase.collection(usage_dict);
        const algorithm_info = await algorithm_collection.findOne({
          _id: new ObjectId(algorithm_hash)
        });
        const usage_info = await usage_collection.findOne({
          _id: new ObjectId(usage_hash)
        });
        if (!algorithm_info || !usage_info) {
          res
            .status(400)
            .send("Algorithm/Usage hash is not found in the  dictionaries!");
          return;
        }

        let versions = {
          new_major: 1,
          major_versions: {
            "0": {
              new_minor: 0,
              minor_versions: {}
            },
            "1": {
              new_minor: 0,
              minor_versions: {}
            }
          }
        };
        req.body["versions"] = versions;
      }
    } else if (coll == evaluation_dict) {
      let dataset_hash = req.body.dataset;
      if (!dataset_hash) {
        req.body["dataset"] = "";
      } else {
        if (!mongodb.ObjectID.isValid(dataset_hash)) {
          res.status(400).send("Dataset hash is not valid!");
          return;
        }
        const dataset_collection = dbase.collection(dataset_dict);
        const dataset_info = await dataset_collection.findOne({
          _id: new ObjectId(dataset_hash)
        });
        if (!dataset_info) {
          res
            .status(400)
            .send("Dataset hash is not found in the dictionaries!");
          return;
        }
      }
    }

    target_collection.insertOne(req.body, (dberr: any, dbres: any) => {
      if (dberr) throw dberr;
      console.log("Insert succeeded!");
      db.close();
      res.status(200).send(dbres.ops[0]); // register succeeded & return posted definition.
    });
  }
};

export const dbDeleteDefinitionByID = async function(
  req: Request,
  res: Response,
  coll: string
) {
  const selfID = getSelfID(res);
  const _id = req.query._id;
  if (!mongodb.ObjectID.isValid(_id)) {
    res.status(400).send("Not valid _id!");
  } else {
    const db = await mongo.connect(url);
    const dbase = await db.db(db_name);
    const account_info = await getAccountInfo(selfID);
    const target_collection = dbase.collection(coll);
    const definition_info = await target_collection.findOne({
      _id: new ObjectId(_id)
    });
    if (!definition_info) {
      db.close();
      res.status(404).send("Definition not found!");
      return;
    }
    console.log("account_info.ID = " + account_info.ID);
    console.log("definition_info.owner_id = " + definition_info.owner_id);

    if (
      canDeleteModel(
        account_info.MM_Level,
        account_info.ID == definition_info.owner_id
      )
    ) {
      if (await isIdleDocument(dbase, _id, coll)) {
        await target_collection.deleteOne(
          { _id: new ObjectId(_id) },
          (dberr: any, dbres: any) => {
            db.close();
            console.log("Delete Succeeded!");
            res.status(200).send("Delete Succeeded!");
          }
        );
      } else {
        res.status(405).send("Definition is not idle, can't be deleted!");
      }
    } else {
      db.close();
      res.status(405).send("Method not allowed!");
    }
  }
};

export const dbPutDefinitionByID = async function(
  req: Request,
  res: Response,
  coll: string
) {
  const selfID = getSelfID(res);
  const _id = req.body._id;
  if (!mongodb.ObjectID.isValid(_id)) {
    res.status(400).send("Not valid _id!");
  } else {
    const db = await mongo.connect(url);
    const dbase = await db.db(db_name);
    const account_info = await getAccountInfo(selfID);
    const target_collection = dbase.collection(coll);
    const definition_info = await target_collection.findOne(
      { _id: new ObjectId(_id) },
      { projection: { _id: 0 } }
    );
    if (!definition_info) {
      db.close();
      res.status(404).send("Definition not found!");
      return;
    }
    console.log("account_info.ID = " + account_info.ID);
    console.log(
      "definition_info.owner_id = " + definition_info.owner_id + "\n"
    );

    if (canModifyModel(account_info.MM_Level)) {
      let target_definition = req.body;
      delete target_definition["_id"];
      const target_name_info = await target_collection.findOne({
        name: target_definition.name
      });

      // If name exists, check duplication
      if (!target_definition.name || !target_name_info) {
        // Check validation of hash if exists
        if (await checkValidationHash(dbase, target_definition, coll)) {
          await target_collection.updateOne(
            { _id: new ObjectId(_id) },
            { $set: target_definition },
            (dberr: any, dbres: any) => {
              db.close();
              console.log("Update Succeeded!");
              res.status(200).send("Update Succeeded!");
            }
          );
        } else {
          db.close();
          res.status(405).send("Target definition hash is not valid!");
        }
      } else {
        db.close();
        res.status(405).send("Duplication name already exists!");
      }
    } else {
      db.close();
      res.status(405).send("Method not allowed!");
    }
  }
};

export const checkValidationHash = async function(
  dbase: any,
  target_def: any,
  coll: string
) {
  const data_type_collection = dbase.collection(data_type_dict);
  const algorithm_collection = dbase.collection(algorithm_dict);
  const usage_collection = dbase.collection(usage_dict);
  const model_type_collection = dbase.collection(model_type_dict);
  const evaluation_collection = dbase.collection(evaluation_dict);
  const dataset_collection = dbase.collection(dataset_dict);
  switch (coll) {
    case efficiency_dict:
    case data_type_dict:
    case algorithm_dict:
    case dataset_dict:
      return true;
    case evaluation_dict:
      if (target_def.dataset) {
        if (!mongodb.ObjectID.isValid(target_def.dataset)) {
          return false;
        }
        const dependency_dataset = await dataset_collection.findOne({
          _id: new ObjectId(target_def.dataset)
        });
        if (!dependency_dataset) {
          return false;
        }
      }
      return true;
    case usage_dict:
      if (target_def.input_type) {
        if (!mongodb.ObjectID.isValid(target_def.input_type)) {
          return false;
        }
        const dependency_in = await data_type_collection.findOne({
          _id: new ObjectId(target_def.input_type)
        });
        if (!dependency_in) {
          return false;
        }
      }
      if (target_def.output_type) {
        if (!mongodb.ObjectID.isValid(target_def.output_type)) {
          return false;
        }
        const dependency_out = await data_type_collection.findOne({
          _id: new ObjectId(target_def.output_type)
        });
        if (!dependency_out) {
          return false;
        }
      }
      return true;
    case model_type_dict:
      if (target_def.algorithm) {
        if (!mongodb.ObjectID.isValid(target_def.algorithm)) {
          return false;
        }
        const dependency_algorithm = await algorithm_collection.findOne({
          _id: new ObjectId(target_def.algorithm)
        });
        if (!dependency_algorithm) {
          return false;
        }
      }
      if (target_def.usage) {
        if (!mongodb.ObjectID.isValid(target_def.usage)) {
          return false;
        }
        const dependency_usage = await usage_collection.findOne({
          _id: new ObjectId(target_def.usage)
        });
        if (!dependency_usage) {
          return false;
        }
      }
      return true;

    // For future Collection.Models checking
    case Collection.Models:
      if (target_def.model_knowledge) {
        const mkn = target_def.model_knowledge;
        if (mkn.intrinsics && mkn.intrinsics.model_type) {
          if (!mongodb.ObjectID.isValid(mkn.intrinsics.model_type)) {
            return false;
          }
          const dependency_usage = await model_type_collection.findOne({
            _id: new ObjectId(mkn.intrinsics.model_type)
          });
          if (!dependency_usage) {
            return false;
          }
        }
        if (mkn.source) {
          if (mkn.source.dataset && mkn.source.dataset != "") {
            if (!mongodb.ObjectID.isValid(mkn.source.dataset)) {
              return false;
            }
            const dependency_dataset = await dataset_collection.findOne({
              _id: new ObjectId(mkn.source.dataset)
            });
            if (!dependency_dataset) {
              return false;
            }
          }
        }
        if (mkn.summary) {
          if (mkn.summary.evaluations) {
            for (const eva in mkn.summary.evaluations) {
              if (!mongodb.ObjectID.isValid(eva)) {
                return false;
              }
              const dependency_usage = await evaluation_collection.findOne({
                _id: new ObjectId(eva)
              });
              if (!dependency_usage) {
                return false;
              }
            }
          }
          if (mkn.summary.efficiencies) {
            for (const eff in mkn.summary.efficiencies) {
              if (!mongodb.ObjectID.isValid(eff)) {
                return false;
              }
              const dependency_usage = await evaluation_collection.findOne({
                _id: new ObjectId(eff)
              });
              if (!dependency_usage) {
                return false;
              }
            }
          }
        }
      }
      return true;
    default:
      return true;
  }
};

export const isIdleDocument = async function(
  dbase: any,
  hash: string,
  coll: string
) {
  console.log("hash: " + hash);
  console.log("target collection: " + coll);
  const model_repo_collection = dbase.collection(Collection.Models);
  const usage_collection = dbase.collection(usage_dict);
  const model_type_collection = dbase.collection(model_type_dict);
  const evaluation_collection = dbase.collection(evaluation_dict);
  let dependency = null;
  let key = null;
  switch (coll) {
    case evaluation_dict:
      key = "model_knowledge.summary.evaluations." + hash;
      dependency = await model_repo_collection.findOne({
        [key]: { $exists: true }
      });
      break;
    case efficiency_dict:
      key = "model_knowledge.summary.efficiencies." + hash;
      dependency = await model_repo_collection.findOne({
        [key]: { $exists: true }
      });
      break;
    case model_type_dict:
      key = "model_knowledge.intrinsics.model_type";
      dependency = await model_repo_collection.findOne({ [key]: hash });
      break;
    case data_type_dict:
      dependency = await usage_collection.findOne({
        $or: [{ input_type: hash }, { output_type: hash }]
      });
      break;
    case algorithm_dict:
      key = "algorithm";
      dependency = await model_type_collection.findOne({ [key]: hash });
      break;
    case usage_dict:
      key = "usage";
      dependency = await model_type_collection.findOne({ [key]: hash });
      break;
    case dataset_dict:
      key = "model_knowledge.source.dataset";
      dependency = await model_repo_collection.findOne({ [key]: hash });
      console.log("Dataset dependency 1:");
      console.log(dependency);
      if (dependency) break;
      key = "dataset";
      dependency = await evaluation_collection.findOne({ [key]: hash });
      console.log("Dataset dependency 2:");
      console.log(dependency);
      break;
    default:
      break;
  }
  if (!dependency) {
    console.log("True");
    console.log(dependency);
    return true;
  } else {
    console.log("False");
    console.log(dependency);
    return false;
  }
};

export const getVersion = function(mtd: any, model_ID: string): string {
  const ma_vers = mtd.versions.major_versions;
  for (const major_version in ma_vers) {
    console.log("major_version: " + major_version);
    const mi_vers = ma_vers[parseInt(major_version)].minor_versions;
    for (const minor_version in mi_vers) {
      const hash = mi_vers[parseInt(minor_version)];
      console.log(
        "    minor_version: " + minor_version + ", model_ID: " + hash
      );
      if (model_ID == hash) {
        console.log("    ......Matched!");
        return (
          "versions.major_versions." +
          major_version +
          ".minor_versions." +
          minor_version
        );
      }
    }
  }
  return "not found";
};

export const getAccountInfo = function(selfID: string) {
  return dbConnection
    .collection(Collection.Accounts)
    .findOne({ ID: selfID }, { projection: { _id: 0 } });
};
