import * as db from "./db";
import { getSelfID } from "./auth";
import { NextFunction } from "express-serve-static-core";
import { Request, Response } from "express";

// Get account's information from GET /v1/account
export const getAccount = function(
  req: Request,
  res: Response,
  next: NextFunction
) {
  console.log("\nGET /v1/account, req.query.ID:");
  console.log(req.query);
  const selfID = getSelfID(res);
  const queryID = req.query.ID;
  if (queryID) {
    db.dbGetAccount(selfID, queryID, res);
  } else {
    next();
  }
};

// Delete account from DELETE /v1/account
export const deleteAccount = function(req: Request, res: Response) {
  console.log("\nDELETE /v1/account, req.query:");
  console.log(req.query);
  const selfID = getSelfID(res);
  const targetID = req.query.ID;
  db.dbDeleteAccount(selfID, targetID, res);
};

// Get all accounts' information from GET /v1/account/list
export const getAccountList = function(req: Request, res: Response) {
  console.log("\nGET /v1/account/list");
  const selfID = getSelfID(res);
  db.dbGetAccountList(selfID, res);
};

// Update levels from PUT /v1/account/level
export const putAccountLevel = async function(req: Request, res: Response) {
  console.log("\nPUT /v1/account/level");
  let targetID = req.body.ID;
  db.dbPutLevel(targetID, req, res);
};
