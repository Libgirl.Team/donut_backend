import * as qs from "querystring";
import { TMLv, MMLv, DMLv } from "./level";
import { Request, Response, NextFunction } from "express";
import config from "config";
import JWT from "../utils/token";

import DBConnection, { Collection } from "./dbConnection";

const fetch = require("node-fetch"); // keep using require node-fetch because @type/node-fetch got different API
import gcpClient from "../auth/gcpClient";
import Users from "../auth/Users";

export let session = require("express-session");
const MongoStore = require("connect-mongo")(session);

// oauth2 configuration
var client_id = config.get("oauth.gcp.web.client_id") as string;
var client_secret = config.get("oauth.gcp.web.client_secret") as string;
if (!client_id || !client_secret)
  throw new Error("OAuth secrets not set in config/ files, cannot run!");
export const redirect_path = "/v1/oauth/oauth2callback";
const redirect_uri = `http://${process.env.HOST || "localhost"}:${process.env
  .PORT || 3000}${redirect_path}`;

// db configuration
const db_url = config.get("db.url");

interface SessionUserInfo {
  gcp_access_token: string;
  ID: string;
  name: string;
}

export class Acnt_Pub_Info {
  constructor(
    public readonly ID: string,
    public readonly name: string,
    public readonly TM_Level: TMLv,
    public readonly MM_Level: MMLv,
    public readonly DM_Level: DMLv
  ) {}
}

export class Account {
  constructor(
    public ID: string,
    public name: string,
    public is_alive: boolean,
    public TM_Level: TMLv = 0,
    public MM_Level: MMLv = 0,
    public DM_Level: DMLv = 0
  ) {}

  static newFromDb(account: Account): Account {
    return <any>Object.assign(new Account("", "", true), account);
  }

  getAccountPublicInfo(): Acnt_Pub_Info {
    return new Acnt_Pub_Info(
      this.ID,
      this.name,
      this.TM_Level,
      this.MM_Level,
      this.DM_Level
    );
  }
}

export const session_setting = {
  secret: process.env.SESSION_SETTING_SECRET || "secret",
  store: new MongoStore({
    url: db_url,
    stringify: false
  }),
  resave: true,
  saveUninitialized: false,
  unset: "destroy",
  cookie: {
    httpOnly: true,
    secure: false,
    maxAge: 1000 * 60 * 60 * 24
  }
};

export const getOAuth2Callback = async function(req: Request, res: Response) {
  const accessToken = await gcpClient.getAccessToken(req.query.code);
  const userInfo = await gcpClient.getUserInfo(accessToken);
  if (accessToken && userInfo.email && userInfo.name) {
    const user = await Users.findUserById(userInfo.email);

    if (user?.is_alive) {
      // found existing account. not deleted. update account profile
      await DBConnection.collection(Collection.Accounts).updateOne(
        { ID: userInfo.email },
        { $set: { name: userInfo.name } }
      );
      res.cookie("access_token", JWT.issue(user as any), {
        maxAge: config.get("session.maxAge"),
        httpOnly: true,
        secure: config.get("session.secure")
      });
      console.log("Successfully updated account info");
    } else if (user && !user.is_alive) {
      // found existing account. but it was deleted by super account. refuse login with 401
      res.clearCookie("access_token");
      res.status(401).end();
    } else {
      // no existing account for the user ID, create new account
      let account = new Account(
        req!.session!.user.ID,
        req!.session!.user.name,
        true
      );
      await DBConnection.collection(Collection.Accounts).insertOne(account);
      console.log("Persisted new user data");
    }
    res.set("Content-Type", "text/html");
    res
      .status(200)
      .send('<script>window.open("","_self").close();</script></html>');
  } else {
    console.log("Failed to get user info from Google");
    res.status(500).send("Oauth Failed");
  }
};

export const authorize = async function(
  req: Request,
  res: Response,
  next: NextFunction
) {
  console.log("\nAuthorizing");

  if (req!.session!.user) {
    const account_info = await DBConnection.collection(
      Collection.Accounts
    ).findOne({ ID: req!.session!.user.ID }, { projection: { is_alive: 1 } });
    if (account_info.is_alive) {
      next();
    } else {
      delete req.session;
      res.status(403).end();
    }
  } else {
    console.log("login required");
    res.status(401).send("login required");
  }
};

export const selfInfo = async function(req: Request, res: Response) {
  if (req!.session!.user) {
    const account_info = await DBConnection.collection(
      Collection.Accounts
    ).findOne({ ID: req!.session!.user.ID }, { projection: { _id: 0 } });
    res.status(200).send(account_info);
  } else {
    res.status(401).send("login required");
  }
};

export const getSelfID = function(res: Response): string {
  if (res?.locals?.user) {
    return res?.locals?.user?.ID;
  } else {
    throw new Error(
      "No user session and user info detected. Failed to get user ID"
    );
  }
};

export const logOut = function(req: Request, res: Response) {
  console.log("!!logout");
  if (req.session) {
    delete req.session;
  }
  res.status(204).end();
};
