import DBConnection, { Collection } from "../utils/dbConnection";
import JWT from "../utils/token";
import { Account } from "../utils/auth";

export default class {
  static async findUserById(id: string): Promise<Account | null> {
    return DBConnection.collection(Collection.Accounts).findOne(
      { ID: id },
      { projection: { _id: 0 } }
    );
  }

  static async authorizeByJWT(token: string): Promise<Account | null> {
    const payload = await JWT.verify(token);
    if (!payload) return null;
    return DBConnection.collection(Collection.Accounts).findOne(
      { ID: payload.sub },
      {}
    );
  }
}
