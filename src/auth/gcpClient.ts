import axios from "axios";
import qs from "querystring";
import { Request, Response } from "express";
import config from "config";
import { TMLv, MMLv, DMLv } from "../utils/level";
import JWT from "../utils/token";
import User, { IUserDocument } from "../models/User";

var client_id = config.get("oauth.gcp.web.client_id") as string;
var client_secret = config.get("oauth.gcp.web.client_secret") as string;

// google oauth2 api
const TOKEN_URI = "https://www.googleapis.com/oauth2/v4/token";
const USERINFO_URI = "https://www.googleapis.com/oauth2/v3/userinfo";
const redirect_path = "/v1/oauth/oauth2callback";
const REDIRECT_URI = `http://${process.env.HOST || "localhost"}:${process.env
  .PORT || 3000}${redirect_path}`;

export class Acnt_Pub_Info {
  constructor(
    public readonly ID: string,
    public readonly name: string,
    public readonly TM_Level: TMLv,
    public readonly MM_Level: MMLv,
    public readonly DM_Level: DMLv
  ) {}
}

export class Account {
  constructor(
    public ID: string,
    public name: string,
    public is_alive: boolean,
    public TM_Level: TMLv = 0,
    public MM_Level: MMLv = 0,
    public DM_Level: DMLv = 0
  ) {}

  static newFromDb(account: Account): Account {
    return <any>Object.assign(new Account("", "", true), account);
  }

  getAccountPublicInfo(): Acnt_Pub_Info {
    return new Acnt_Pub_Info(
      this.ID,
      this.name,
      this.TM_Level,
      this.MM_Level,
      this.DM_Level
    );
  }
}

export default class GCPClient {
  static async getAccessToken(code: string) {
    const res = await axios(TOKEN_URI, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: qs.stringify({
        client_id,
        client_secret,
        code,
        grant_type: "authorization_code",
        redirect_uri: REDIRECT_URI
      })
    });
    return res.data.access_token;
  }

  static async getUserInfo(token: string) {
    const res = await axios(USERINFO_URI, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
    return res.data;
  }

  static closeTab(res: Response) {
    res.set("Content-Type", "text/html");
    res
      .status(200)
      .send('<script>window.open("","_self").close();</script></html>');
  }

  static grantAccess(res: Response, user: IUserDocument) {
    res.cookie("access_token", JWT.issue(user), {
      maxAge: config.get("session.maxAge"),
      httpOnly: true,
      secure: config.get("session.secure")
    });
  }

  static async logout(req: Request, res: Response) {
    res.clearCookie("access_token");
    return res.status(204).end();
  }

  static denyAccess(res: Response) {
    res.clearCookie("access_token");
    return res.status(401).end();
  }

  static async callback(req: Request, res: Response) {
    const accessToken = await GCPClient.getAccessToken(req.query.code);
    const userInfo = await GCPClient.getUserInfo(accessToken);
    if (accessToken && userInfo.email && userInfo.name) {
      const user = await User.findOrCreate(userInfo.email, userInfo.name);

      if (user && !user.active) GCPClient.denyAccess(res);
      if (user) GCPClient.grantAccess(res, user);
      GCPClient.closeTab(res);
    } else {
      console.log("Failed to get user info from Google");
      res.status(500).send("Oauth Failed");
    }
  }
}
