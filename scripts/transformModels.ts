import mongoose from "mongoose";
import Model from "../src/models/Model";
import User from "../src/models/User";
import ModelType from "../src/models/ModelType";

const convertUser = (m: any) => ({
  _id: m._id,
  active: m.is_alive,
  mmLevel: m.MM_Level,
  dmLevel: m.MM_Level,
  tmLevel: m.TM_Level,
  name: m.name,
  email: m.ID
});

const convertModelType = (m: any, users: any[]) => {
  const { name, owner_id, description, _id } = m;
  const ownerId = users.find(u => u.ID === owner_id)?._id;
  return {
    _id,
    name,
    description,
    ownerId
  };
};

const convertModel = (m: any, users: any[]) => {
  const { address, description, owner_id } = m.model_knowledge;
  const ownerId = users.find(u => u.ID === owner_id)?._id;
  return {
    _id: m._id,
    modelURI: address,
    description: description,
    ownerId,
    authorId: null,
    modelTypeId: m.model_knowledge.intrinsics.model_type
  };
};

mongoose
  .connect("mongodb://localhost:27017/donut_dev", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    dbName: "donut_dev"
  })
  .then(async ({ connection: conn }) => {
    mongoose.set("debug", true);

    const getCollection = (collection: string) => {
      return conn
        .collection(collection)
        .find({})
        .toArray();
    };

    const dropCollection = async (collection: string) => {
      await conn
        .dropCollection(collection)
        .then(() => console.log(`Dropped collection ${collection}`))
        .catch(() => console.log(`Could not drop collection ${collection}`));
    };

    const users = await getCollection("Account_Repo");

    const convertCollection = async (
      collection: string,
      model: any,
      converterFunction: (m: any, userList: any[]) => any
    ) => {
      await dropCollection(model.collection.name);
      await getCollection(collection)
        .then(items => items.map(item => converterFunction(item, users)))
        .then(items => model.insertMany(items));
    };

    await convertCollection("Account_Repo", User, convertUser);
    await convertCollection("ModelType_Dict", ModelType, convertModelType);
    await convertCollection("Model_Repo", Model, convertModel);

    process.exit(0);
  });
