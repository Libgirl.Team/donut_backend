# Donut Model Management Json Example

## Introduction

This is the model management json example for collections.


## Account_Repo

You don't have to create the account documents by yourself.
Just use oath to login Donut (temporarily please visit the experimental http://localhost:3000/, your account record will be saved into Account_Repo collection like the examples.

### Examples
```json
{
    "_id" : ObjectId("your-account-object-id"),
    "ID" : "scchen@libgirl.com",
    "name" : "Shih-Chia Chen",
    "is_alive" : true,
    "TM_Level" : 0,
    "MM_Level" : 0,
    "DM_Level" : 0
}
```


