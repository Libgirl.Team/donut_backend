
# At the Beginning

This is the model management json example for collections.

In each collection, a document would represents a different unit and would be listed.

In typescript, use type alias for the labels & items:

```javascript
// type Usage_Label = String;
```

All examples prefix with hash would be an ObjectId of mongoDB and should be linked to the definition of the related library's "_id".

# Model_Repo

> Each document in collection Model_Repo is a Model
>
> Notice:
>
> 1. prev_model could be empty, which means no previous model reference is given.
>
> 2. parameters mean hyperparameters using in this model's training
> 3. dataset refers to the dataset in the Dataset_Dict, means the data used in this model's training
> 4. about just does the same as what the previous description does.
>
>

```json
{
    "_id" : ObjectId("hash_of_model_resnet18_chocolate_v_1_0"),
    "model_knowledge" : {
        "owner_id" : "balisun@libgirl.com",
        "author_id" : "cy@libgirl.com",
        "description" : "Chocolate recommendation resnet18 trained by additional Singapore dataset.",
        "address" : "gs://chocolate-recommendation/rnn-2019-10-15.h5",
        "source" : {
            "prev_model": "hash_of_model_resnet18_chocolate_v_0_3",
            "parameters": "(string)",
            "dataset":"hash_of_cifar10",
        },
        "intrinsics" : {
            "model_type" : "hash_of_resnet18_chocolate-recommendation"
        },
        "summary" : {
            "evaluations" : {
                "hash_of_precision"  : 0.87,
                "hash_of_recall" : 0.78,
                "hash_of_f1" : 0.85
            },
            "efficiencies" : {
                "hash_of_min_memory" : 512,
                "hash_of_response_time" : 200
            }
        }
    }
}
```

> the request body of POST /model : Model_Knowledge and a Major Version Number
>
> A POST action of a model should determine major version, named "major_number".

```json
{
    "major_number" : 1,
    "model_knowledge" : {
        "owner_id" : "balisun@libgirl.com",
        "author_id" : "cy@libgirl.com",
        "description" : "Chocolate recommendation resnet18 trained by additional Singapore dataset.",
        "address" : "gs://chocolate-recommendation/rnn-2019-10-15.h5",
        "source" : {
            "prev_model": "hash_of_model_resnet18_chocolate_v_0_3",
            "parameters": "(string)",
            "dataset":"hash_of_cifar10",
        },
        "intrinsics" : {
            "model_type" : "hash_of_resnet18_chocolate-recommendation"
        },
        "summary" : {
            "evaluations" : {
                "hash_of_precision"  : 0.87,
                "hash_of_recall" : 0.78,
                "hash_of_f1" : 0.85
            },
            "efficiencies" : {
                "hash_of_min_memory" : 512,
                "hash_of_response_time" : 200
            }
        }
    }
}
```

# ModelType_Dict

> Each document in collection ModelType_Dict is a Model_Type_Definition
>
> For a first-time established Model_Type_Definition, the definition should still include versions 0 and 1, with new_major 1 and new_minor 0. (Backend server would handle it)

```json
{
    "_id" : ObjectId("hash_of_resnet18_chocolate-recommendation"),
    "owner_id" : "blu@libgirl.com",
    "name" : "resnet18-chocolate",
    "description" : "make recommendation (like or not) by resnet18 based on input image.",
    "algorithm" : "hash_of_resnet18",
    "usage" : "hash_of_chocolate_recommendation",
    "versions" : {
        "new_major" : 1,
        "major_versions" : {
            "0" : {
                "new_minor" : 2,
                "minor_versions" : {
                    "0" : "hash_of_model_resnet18_chocolate_v_0_0",
                    "1" : "hash_of_model_resnet18_chocolate_v_1_1"
                }
            },
            "1" : {
                "new_minor" : 0,
                "minor_versions" : {
                    "0" : "hash_of_model_resnet18_chocolate_v_1_0"
                }
            }
        }
    }
}
```



# Algorithm_Dict

> Each document in collection Algorithm_Dict is a Algorithm_Definition

```json
{
    "_id" : ObjectId("hash_of_resnet18"),
    "name" : "resnet18",
    "owner_id" : "cy@libgirl.com",
    "description" : "CNN resnet18. reference paper: akgjawdlkgjadsgla."
}
```

# Usage_Dict

> Each document in collection Usage_Dict is a Usage_Definition

```json
{
    "_id" : ObjectId("hash_of_chocolate_recommendation"),
    "name" : "chocolate-recommendation",
    "owner_id" : "chocolate_lover@libgirl.com",
    "input_type" : "hash_of_img",
    "output_type" : "hash_of_bool",
    "description" : "take input image and output whether the chocolate will be popular or not."
}
```

# DataType_Dict

> Each document in collection DataType_Dict is a DataType_Definition

```json
{
    "_id" : ObjectId("hash_of_img"),
    "name" : "img",
    "owner_id" : "balisun@libgirl.com",
    "description" : "array of pixels of 3 color channels."
}

{
    "_id" : ObjectId("hash_of_bool"),
    "name" : "bool",
    "owner_id" : "blu@libgirl.com",
    "description" : "1 / 0 = true / false."
}
```

# Evaluation_Dict

> Each document in collection Evaluation_Dict is a Evaluation_Definition.
>
> Notice: If dataset value is NOT included while establishing,
> it will be set as empty string "" in the database.
> Also, duplication of "name" will NOT be allowed.

```json
{
    "_id" : ObjectId("hash_of_precision_at_cifar10"),
    "name" : "precision @ cifar10",
    "owner_id" : "shhsu@libgirl.com",
    "unit" : "A.U.",
    "description" : "precision. google it yourself.",
    "dataset" : "5dxebcfb7b33ec6671cfd1f5"
}

{
    "_id" : ObjectId("hash_of_recall"),
    "name" : "recall @ cifar10",
    "owner_id" : "shhsu@libgirl.com",
    "unit" : "A.U.",
    "description" : "recall. google it yourself.",
    "dataset" : "5dxebcfb7b33ec6671cfd1f5"
}

{
    "_id" : ObjectId("f1"),
    "name" : "f1 @ cifar10",
    "owner_id" : "shhsu@libgirl.com",
    "unit" : "A.U.",
    "description" : "not automotive racing.",
    "dataset" : "5dxebcfb7b33ec6671cfd1f5"
}
```

# Efficiency_Dict

> Each document in collection Efficiency_Dict is a Efficiency_Definition

```json
{
    "_id" : ObjectId("hash_of_min_memory"),
    "name" : "min_memory",
    "owner_id" : "cy@libgirl.com",
    "unit" : "MB",
    "description" : "minimum memory required for the device for working smoothly."
}

{
    "_id" : ObjectId("hash_of_response_time"),
    "name" : "response_time",
    "owner_id" : "cy@libgirl.com",
    "unit" : "ms",
    "description" : "response time by standard measurement in millisecond."
}
```

# Dataset_Dict

> Each document in collection Dataset_Dict is a Dataset_Definition

```json
{
    "_id" : ObjectId("hash_of_cifar10"),
    "name" : "cifar10",
    "owner_id" : "chihyulin@libgirl.com",
    "url" : "gs://somewhere.over.the.rainbow",
    "description" : "Cifar-10 dataset, in grayscale format"
}
```
