# Donut Backend

## Installation

Use [yarn](https://yarnpkg.com/en/) to install and update dependencies

## Install/Update Dependencies

See [yarn usage](https://yarnpkg.com/en/docs/usage)

## Environments

### Downloading GCP Oauth Secret

Please find and [download the gcp oauth secret from here](https://console.cloud.google.com/apis/credentials?project=donut-256801&organizationId=400781712235)

![Where to download oauth secret](https://gitlab.com/Libgirl.Team/donut_backend/raw/5a34f4bc8c86c9cd9b800bc4e32fa56534f11fd9/docs/getting_gcp_oauth_secret.png)

### Local Environment Variables
You can use ```.env``` file like the following and put under **project root**.
```
GCP_SECRET_PATH=/path-to-your-local-gcp-oauth-secret-you-just-downloaded
SESSION_SETTING_SECRET=ANY_RANDOM_STRING
DB_URL=your-db-url-such-as-mongodb://localhost:27017/test
DB_NAME=your-db-name-such-as-test
```
Or you can directly set environment variables

## Testing with Postman
### Sync Chrome's Cookies to Postman to Pass Authorization

NOTICE: Please use **Chrome**, **NOT** Chromium.
#### Steps
##### Install [Postman Interceptor](https://learning.getpostman.com/docs/postman/sending_api_requests/interceptor_extension/)

1. Install Postman Interceptor
2. Install Postman Interceptor Chrome Extension

Please relaunch Postman and Chrome after your installation.

##### [syncing cookies](https://learning.getpostman.com/docs/postman/sending_api_requests/interceptor_extension/#syncing-cookies)

On Capture Cookies page (See below for figures)

1. Make sure Interceptor is connected (green light)
1. Enter a domain to capture cookie. If you're testing localhost, add localhost. NOT localhost:3000.
1. Keep your Postman open.
1. Login your testing @libgirl.com account at http://localhost:3000/ with Chrome. 
1. Starting from your next request to localhost with Postman, the Cookies with name connect.sid should be available to use. 

![Interceptor is connected and add localhost for sync](https://gitlab.com/Libgirl.Team/donut_backend/raw/7ccc986dc7b36a4ea3588c3f73e8234427ee3aaf/docs/postman_interceptor_1.png)
![Find cookie management](https://gitlab.com/Libgirl.Team/donut_backend/raw/a548224c3323d0feffe0d26b27cddd163e3c388c/docs/postman_interceptor_2.png)
![Cookie name is connect.sid](https://gitlab.com/Libgirl.Team/donut_backend/raw/a548224c3323d0feffe0d26b27cddd163e3c388c/docs/postman_interceptor_3.png)

## Usage

```
$ npm start
```
